﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using TestGenerator;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var setOfTask = new SetOfTasks();
            for (var j = 0; j < 3; j++)
            {
                Console.WriteLine("input caption of task");
                var caption = Console.ReadLine();
                Console.WriteLine("input info of task");
                var infoTask = Console.ReadLine();
                var variants = new List<Variant>();
                for (var i = 0; i < 4; i++)
                {
                    Console.WriteLine("input variant info");
                    var info = Console.ReadLine();
                    Console.WriteLine("input is correct ( true false)");
                    var correct = bool.Parse(Console.ReadLine());
                    variants.Add(Factory.CreateVariant(info, correct));
                }
                var task = Factory.CreateTask(variants, caption, infoTask);
                Console.WriteLine(task.ToString());
                Console.ReadKey();
                setOfTask.Tasks.Add(task);
            }

            var test = setOfTask.GenerateTest(2);

            const string filename = "MyTest.xml";
            var serializer = new XmlSerializer(typeof(SetOfTasks));
            if (File.Exists(filename))
                File.Delete(filename);
            using (var fs = File.Create(filename))
            {
                serializer.Serialize(fs, setOfTask);
            }

            foreach (var testTask in test.Tasks)
            {
                Console.WriteLine(testTask);
            }
        }
    }
}
