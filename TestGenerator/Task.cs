﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestGenerator
{
    [Serializable]
    public class Task
    {
        public List<Variant> Variants { get; set; }
        public string Caption { get; set; }
        public string Info { get; set; }
        public override string ToString()
        {
            var result = string.Empty;
            foreach (var variant in Variants)            
                result += variant.Info + " " + variant.IsCorrect + "\n";

            result += "Caption " + Caption + "\n";
            result += "Info " + Info + "\n";
            return result;
        }
    }
}
