﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestGenerator
{
    [Serializable]
    public class Variant
    {
        public string Info { get; set; }
        public bool IsCorrect { get; set; }
        public bool UserInput { get; set; }
    }
}
