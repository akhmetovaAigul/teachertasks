﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestGenerator
{
    public class Factory
    {
        public static Variant CreateVariant(string info, bool isCorrect)
        {
            var variant = new Variant()
            {
                Info = info,
                IsCorrect = isCorrect
            };
            return variant;
        }

        public static Task CreateTask(List<Variant> variants, string caption, string info)
        {
            var task = new Task()
            {
                Variants = variants,
                Caption = caption,
                Info = info
            };
            return task;
        }

        public static SetOfTasks CreateSetOfTasks(List<Task> tasks)
        {
            return new SetOfTasks { Tasks = tasks };
        }
        
    }
}
