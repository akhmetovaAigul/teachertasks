﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestGenerator
{
    [Serializable]
    public class SetOfTasks
    {
        private readonly Random _random = new Random();
        public List<Task> Tasks { get; set; } = new List<Task>();

        public Test GenerateTest(int numberTasks)
        {
            if (numberTasks > Tasks.Count)
                throw new ArgumentException("Too much number");
            var test = new Test();
            var list = Tasks.ToList();
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = _random.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
            for (var i = 0; i < numberTasks; i++)
                test.Tasks.Add(list[i]);
            return test;
        }
    }
}
