﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestGenerator
{
    [Serializable]
    public class Test
    {
        public List<Task> Tasks { get; set; } = new List<Task>();
    }
}
